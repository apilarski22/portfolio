import React, { Component } from 'react';
import '../css/style.scss';
import { Link } from 'react-router-dom';
import github from '../img/svg/github.svg';
import loader from '../img/svg/tailspin.svg';
import { CSSTransition} from 'react-transition-group';
import logo from '../img/logo.png';






class Contact extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loadedComponent: false,
            loaderVisible: true,
            loaded: false
        }

    }

    componentDidMount() {
        
        setTimeout(() => {
            this.setState({loadedComponent: true}, () => {
                setTimeout(() => {
                    this.setState({ loaded: true, loaderVisible: false})
                },1500)
            })
        }, 1500)
      
    }

    render() {
        return (

            <div className="contact">
            {this.state.loaderVisible ? <div className="loader" style={{ opacity: `${this.state.loadedComponent ? '0' : '1'}` }}> <img className="loader__logo" src={logo} alt="logo"></img> <img className="loader__anim" src={loader}></img>
           
                    <h2 className="header--hero header--small">loading...</h2>
                    
                </div> : ""}
                <div className="contact__container">
                <CSSTransition in={this.state.loaded} appear={true} timeout={0} classNames="fade">
                <div className="contact__mail">
                    <p className="header--hero header--list contact__subheader">Skontaktuj się ze mną:</p>
                    <h1 className="header--hero contact__header">pilarskialeksander22@gmail.com</h1>
                    </div>
                </CSSTransition>
                <CSSTransition in={this.state.loaded} appear={true} timeout={700} classNames="fade">
                    <a className="contact__social" href="https://github.com/goeden22">
                    <img src={github} className="contact__icon"></img>
                    <p className="header--small contact__description"> goeden22</p>
                    </a>
                </CSSTransition>
                </div>
            </div>

        );
    }

}



export default Contact;
