import React, { Component } from 'react';
import '../css/style.scss';
import arrdown from '../img/svg/arrdown.svg';







class JumpingArrow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            offset: false
           
        }

    }

    componentDidMount() {
        setInterval(() => {
            this.setState({offset: !this.state.offset})
        },2000)
       
      
    }

    render() {
        return (

           
                <img src={arrdown} className="portfolio__mobileArrow" style={{transform: `translateY(${this.state.offset ? '-40px' : 0})`}}/>
           

        );
    }

}



export default JumpingArrow;
