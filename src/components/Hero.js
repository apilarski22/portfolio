import React, { Component } from 'react';
import '../css/style.scss';
import portret from '../img/portret.jpg';
import logo from '../img/logo.png';
import mainVideo from '../video/main.mp4'
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import jquery from '../img/svg/jquery.svg';
import mongodb from '../img/svg/mongodb.svg';
import sass from '../img/svg/sass.svg';
import webpack from '../img/svg/webpack.svg';
import react from '../img/svg/react.svg';
import html from '../img/svg/html.svg';
import js from '../img/svg/js.svg';
import nodejs from '../img/svg/nodejs.svg';
import git from '../img/svg/git.svg';
import css from '../img/svg/css.svg';
import { throttle } from 'lodash'
import loader from '../img/svg/tailspin.svg';
import {Link} from 'react-router-dom';






class Hero extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            appear: false,
            loadedVideo: false,
            hovered: this.props.hovered,
            scroll: 0,
            firstLogosVisible: false,
            secondLogosVisible: false,
            loaderVisible: true
        }
        this.handleCanPlayThrough = () => {
         
            let video = this.video || undefined
            setTimeout(() => {
                this.setState({ loadedVideo: true }, () => {
                    setTimeout(() => {
                        if (video){
                            video.play();
                        }
                        
                        this.setState({ appear: true, loaderVisible: false })
                    }, 1200)
    
                })
            }, 2500)
            
        }
        this.handleScroll = () => {
            let firstLogos = document.getElementById('firstLogos')
            let secondLogos = document.getElementById('firstLogos')
            this.setState({ scroll: window.scrollY })
            if (firstLogos && secondLogos){
                let firstDistanceFromTop = firstLogos.getBoundingClientRect().top + window.scrollY + firstLogos.offsetHeight
                let secondDistanceFromTop = secondLogos.getBoundingClientRect().top + window.scrollY + secondLogos.offsetHeight
                if (window.scrollY + window.innerHeight > firstDistanceFromTop) {
                    this.setState({ firstLogosVisible: true })
                }
                if (window.scrollY + window.innerHeight > secondDistanceFromTop) {
                    this.setState({ secondLogosVisible: true })
                }
            }
           





        }
    }

    componentDidMount() {
        this.setState({ loaded: true })


        window.addEventListener('scroll', throttle(this.handleScroll, 300))
        
        let videoTimeout = setTimeout(() => {
            
            this.handleCanPlayThrough();
        }, 4500)
        this.video.addEventListener('canplaythrough', () => {
            clearTimeout(videoTimeout)
            this.handleCanPlayThrough();
        } )
    }

    componentWillUnmount() {
        window.removeEventListener('canplaythrough', this.handleCanPlayThrough)
    }
    render() {
        return (

            <div className="hero">
                {this.state.loaderVisible ? <div className="loader" style={{ opacity: `${this.state.loadedVideo ? '0' : '1'}` }}><img className="loader__logo" src={logo} alt="logo"></img> <img className="loader__anim" src={loader}></img>
                
                    <h2 className="header--hero header--small">loading...</h2>
                    
                </div> : ""}
                <CSSTransition in={this.state.appear} appear={true} timeout={0} classNames="fade">
                <div className="hero__first">
                    <div className="hero__main">
                 
                        <div className="hero__photoContainer" style={{ transform: `translateY(-${this.state.scroll / 3}px)` }}>
                            <h1 className="header--hero header--main top">Aleksander Pilarski</h1>
                            <h1 className="header--small header--second top">Portfolio</h1>
                           
                        </div>
                    
                        <div className="hero__movieContainer">
                          
                                <div className="hero__lists" style={{ transform: `translateY(-${this.state.scroll / 3}px)` }}>
                                    <ul className="hero__list">
                                        <li className="header--hero header--list">Webdev</li>
                                        <li className="header--small hero__listItem">Strony internetowe</li>
                                        <li className="header--small hero__listItem">Aplikacje webowe</li>
                                        <li className="header--small hero__listItem">Responsywny design</li>
                                        <li className="header--small hero__listItem">Technologie mobilne</li>
                                    </ul>
                                    <ul className="hero__list">
                                        <li className="header--hero header--list">Grafika</li>
                                        <li className="header--small hero__listItem">Webdesign</li>
                                        <li className="header--small hero__listItem">Identyfikacja wizualna</li>
                                        <li className="header--small hero__listItem">Ilustracja</li>

                                    </ul>

                                </div>
                          
                                <CSSTransition in={this.state.appear} appear={true} timeout={1800} classNames="fade">
                            <Link className="header--small hero__button" to='/portfolio' style={{ transform: `translateY(-${this.state.scroll / 5}px)` }}>
                               zobacz portfolio
                                </Link>
                                </ CSSTransition>
                            <div className="hero__movieFrame" style={{ transform: `translateY(-${this.state.scroll / 12}px)` }} >
                                <video className="hero__movie" autoplay muted loop ref={function (ref) { this.video = ref }.bind(this)}>
                                    <source src={mainVideo} type="video/mp4" />
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
                </ CSSTransition>
                <div className="second">
                    <div className="second__main">
                        <div className="second__photoFrame" style={{ transform: `translateY(-${this.state.scroll / 7}px)` }}>
                        
                            <img src={portret} className="second__photo"></img>

                        </div>

                        <div className="second__text" style={{ transform: `translateY(-${this.state.scroll / 12}px)` }}>
                        <h1 className="header--hero second__header">o mnie</h1>
                            <h1 className="header--hero second__subheader">Moje umiejętności</h1>
                            <div className="second__paragraphs" >
                                <p className="header--small second__paragraph">Jestem początkującym web developerem i grafikiem z Wrocławia, który stara się doskonalić swój warsztat, by kiedyś zająć się projektowaniem aplikacji internetowych na profesjonalnym poziomie. W swoich pracach zawsze staram się podążać za aktualnymi trednami i wzorami projektowania, jednocześnie ulepszając je o swoje autorskie rozwiązania i pomysły.</p>
                                <p className="header--small second__paragraph">Głównym obszarem, w którym się rozwijam jest front-end, aczkolwiek znajomość podstaw technologii back-endowych (node.js, mongoDB), pozwala mi pisać w pełni funcjonalne aplikacje sieciowe zarówno po stronie clienta, jak i serwera.</p>
                                <p className="header--small second__paragraph">W kolejnej sekcji, u dołu strony, wypisałem technologie które opanowałem, zapraszam również do sprawdzenia moich wybranych prac w zakładce portfolio.</p>
                                <p className="header--small second__paragraph">Jestem otwarty na propozycje zatrudnienia, bądź płatnego stażu na terenie Wrocławia, dane kontaktowe do mnie, są dostępne w zakładce kontakt.</p>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="third">
                        <div className="third__container">
                            <h1 className="header--hero header--list third__header">Technologie</h1>
                            <p className="header--small third__subheader">Poniżej wypisałem główne technologie, któe wykorzystuje przy kodowaniu swoich projektów, każdą z nich
                opanowałem w praktyce, w stopniu pozwalającym na swobodne kodowanie w niej i realizacje założeń projektowych.</p>
                            <CSSTransition in={this.state.firstLogosVisible} appear={true} timeout={0} classNames="fade">
                                <div className="third__row" id="firstLogos" ref={function (ref) { this.firstLogos = ref }.bind(this)}>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">HTML</h2><img className="third__img" src={html}></img></div>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">CSS</h2><img className="third__img" src={css}></img></div>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">JS</h2><img className="third__img" src={js}></img></div>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">Sass</h2><img className="third__img" src={sass}></img></div>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">jQuery</h2><img className="third__img" src={jquery}></img></div>
                                </div>
                            </ CSSTransition>
                            <CSSTransition in={this.state.secondLogosVisible} appear={true} timeout={800} classNames="fade">
                                <div className="third__row"  id="secondLogos" ref={function (ref) { this.secondLogos = ref }.bind(this)}>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">React</h2><img className="third__img" src={react}></img></div>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">Node.js</h2><img className="third__img" src={nodejs}></img></div>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">MongoDB</h2><img className="third__img" src={mongodb}></img></div>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">Webpack</h2><img className="third__img" src={webpack}></img></div>
                                    <div className="third__column"><h2 className="header--hero  third__listHeader">Git</h2><img className="third__img" src={git}></img></div>
                                </div>
                            </ CSSTransition>
                        </div>
                    </div>
            </div >

        );
    }

}



export default Hero;
