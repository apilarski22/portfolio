import React, { Component } from 'react';
import '../css/style.scss';
import menu from '../img/svg/menu.svg';
import {Link} from 'react-router-dom';






class Menu extends Component {

    constructor(props) {
        super(props);
        this.state = {
          visible: true
        }
        this.hideMenu = () => {
          
          let newState = this.state.visible ? false : true
          this.setState({visible: newState})
        }
    }

    render() {
        return (

            <div className="menu">
            <div className="menu__button" onClick={this.hideMenu}><img src={menu} className="menu__icon"></img></div>
        <ul className={"menu__list"+ ` ${this.state.visible ? '' : 'menu--visible'}`}>
        <Link to='/'>
          <li className="menu__listItem header--small">o mnie</li>
        </Link>
          <li className="menu__separator">|</li>
        <Link to='/portfolio'>
          <li className="menu__listItem header--small">portfolio</li>
        </Link>
          <li className="menu__separator">|</li>
        <Link to='/contact'>
          <li className="menu__listItem header--small">kontakt</li>
        </Link>
          

        </ul>
      </div>

        );
    }

}



export default Menu;
