import React, { Component } from 'react';
import '../css/style.scss';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import loader from '../img/svg/tailspin.svg';
import arrup from '../img/svg/arrup.svg';
import arrdown from '../img/svg/arrdown.svg';
import { data } from '../data/data.js';
import logo from '../img/logo.png';
import JumpingArrow from './JumpingArrow.js';







class Portfolio extends Component {

    constructor(props) {
        super(props);
        this.state = {
            current: data[0],
            appear: false,
            scaled: false,
            centered: false,
            blocked: true,
            loader: true,
            loading: true,
            loaderAppear: true,
            mobile: true
        }
        this.next = (direction) => {
         
            const dir = direction === 'up' ? -1 : 1
            let next = typeof direction === "number" ? direction : this.state.current.index + dir;
            console.log(next, data.length)
            if (next < 0 || next > data.length - 1 || this.state.blocked) {
                return false
            } else {
                this.setState({ appear: false }, () => {
                    setTimeout(() => {
                        this.setState({ blocked: true, centered: true }, () => {
                            setTimeout(() => {
                                this.setState({ scaled: true },
                                    () => {

                                        this.setState({ current: data[next], loader: true }, () => {
                                            setTimeout(() => {
                                                this.setState({ scaled: false }, () => {
                                                    setTimeout(() => {
                                                        this.setState({ centered: false }, () => {
                                                            this.setState({ appear: true }, () => {
                                                                setTimeout(() => {
                                                                    this.setState({ blocked: false })
                                                                }, 200)
                                                            })

                                                        })
                                                    }, 500);

                                                })
                                            }, 2800)
                                        })


                                    })
                            }, 300)
                        })
                    }, 500)
                })




            }

        }
        this.handleLoad = () => {
            this.setState({ loader: false })
        }
        this.checkIfMobile = () => {
            let mobile = document.querySelector('body').offsetWidth <= 765 ? true : false
            if(this.state.mobile != mobile){
                this.setState({mobile})
            }
        }
    }

    componentDidMount() {
            this.checkIfMobile();
            window.addEventListener("resize", this.checkIfMobile);
            
            setTimeout(() => {
                this.setState({ appear: true, loading: false }, () => {
                    window.scrollTo(0,0);
                    setTimeout(() => {
                        this.setState({ blocked: false, loaderAppear: false })
                       
                    }, 1500)
                });
            }, 2500)
        


    }

    render() {
        let current = this.state.current
        return (

            <div className="portfolio" onResize={this.checkIfMobile}>
            {this.state.loaderAppear ?      <div className="loader" style={{ opacity: `${this.state.loading ? '1' : 0}` }}>
            <img className="loader__logo" src={logo} alt="logo"></img>
                    <img className="loader__anim" src={loader}></img>
                    <h2 className="header--hero header--small">loading...</h2>
                    
                 </div> : ""}
           
                <div className="portfolio__container">
                    <div className="portfolio__entry">
                    {this.state.mobile ?
                                            <CSSTransition in={this.state.appear} appear={true} timeout={500} classNames="fade">
                                            <div className="portfolio__mobileDesc">
                                        <h1 className="header--hero portfolio__header">{current.name}</h1>
                                        <ul className="portfolio__tags">
                                        {current.tags.map((tag) => {
                                            return <li className="portfolio__tag header--tag">{tag}</li>
                                        })}

                                    </ul>
                                    <div className="portfolio__buttonRow">
                                        <a target="_blank" className="header--small portfolio__button" href={current.giturl}>zobacz na github</a>
                                        <a target="_blank" className="header--small portfolio__button" href={current.url}>zobacz aplikacje</a>
                                    </div>
                                    </div>
                                    </CSSTransition> :""}
                        <div className="portfolio__photoContainer" style={{ left: `${(this.state.centered && !this.state.mobile)? '40%' : '0'}` }}>
                            <TransitionGroup>
                                <CSSTransition key={this.state.current.index} timeout={3500} classNames={this.state.direction == "down" ? "fadedown" : "fadeup"}>
                                    
                                    <div className="portfolio__photoFrame">
                                    {this.state.mobile ?<JumpingArrow /> : ""}
                                        <div className="portfolio__photoMasked" style={{ transform: `${this.state.scaled ? "scale(1)" : ''}` }}>
                                       
                                            <div className="portfolio__photoLoader" style={{ opacity: `${this.state.loader ? '1' : 0}` }}><img className="portfolio__photoWheel" src={loader} ></img></div>
                                            <img className="portfolio__photo" src={require(`../img/${current.img}.jpg`)} onLoad={this.handleLoad}></img>
                                        </div>


                                    </div>
                                </ CSSTransition>
                            </ TransitionGroup>
                        </div>




                        <div className="portfolio__textContainer">
                            <CSSTransition in={this.state.appear} appear={true} timeout={1500} classNames="fade">
                                <div className="portfolio__textFrame">
                                    {!this.state.mobile ? <div>
                                    <h1 className="header--hero portfolio__header">{current.name}</h1>
                                    <ul className="portfolio__tags">
                                    
                                        {current.tags.map((tag) => {
                                            return <li className="portfolio__tag header--tag">{tag}</li>
                                        })}

                                    </ul>
                                    <div className="portfolio__buttonRow">
                                        <a target="_blank" className="header--small portfolio__button" href={current.giturl}>zobacz na github</a>
                                        <a target="_blank" className="header--small portfolio__button" href={current.url}>zobacz aplikacje</a>
                                    </div>
                                    </div> : ""}
                                    {current.description.map((paragraph) => {
                                        return <p className="paragraph--main portfolio__paragraph">{paragraph}</p>
                                    })}
                                    
                                </div>
                            </CSSTransition>
                        </div>

                    </div>
                    <div className="portfolioMenu">
                        <button className={`portfolioMenu__button ${this.state.blocked ? 'portfolioMenu__button--blocked' : ''}`} onClick={() => { this.next('up') }}><img className="portfolioMenu__icon" src={arrup} /></button>
                        <ul className="portfolioMenu__list">
                            {data.map(el => {
                                return <li className={` header--tag portfolioMenu__item ${current.name === el.name ? 'activeMenuItem' : ''}`} onClick={() => {this.next(el.index)}}>{el.name}</li>
                            })}
                        </ul>
                        <button className={`portfolioMenu__button ${this.state.blocked ? 'portfolioMenu__button--blocked' : ''}`} onClick={() => { this.next('down') }}><img className="portfolioMenu__icon" src={arrdown} /></button>
                    </div>
                </div>

            </div>

        );
    }

}



export default Portfolio;
