
import React, { Component } from 'react';
import './css/style.scss';

import Hero from './components/Hero.js'
import Menu from './components/Menu.js'
import Contact from './components/Contact.js'
import Portfolio from './components/Portfolio.js'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

class App extends Component {

  constructor(props) {
    super(props);
    

    
  }

  render() {
    return (
      <div className="App">
       
        <Router>
        <Menu />
        <Route render={({location}) => (
          <TransitionGroup>
          <CSSTransition key={location.key} timeout={2000} classNames="fadeSwitch">
            <Switch location={location}>
              <Route path='/' exact component={Hero}/>
              <Route path='/portfolio' component={Portfolio}/>
              <Route path='/contact' component={Contact}/>
              
            </Switch>
            </CSSTransition>
            </TransitionGroup>
        )} />
        
        </Router>
        
         
        
       
      </div>
    );
  }
}

export default App;
