const data = [
    {
        index: 0,
        name: "Climate Map",
        tags: ["React", "Leaflet", "API", "Sass"],
        description: ["Aplikacja z interaktywną mapą, która na podstawie kliknięcia miejsca na mapie, bądź wyszukania go po nazwie, wyszukuje informacje dotyczące rodzaju klimatu i aktualnej pogody panującej na danym terenie. Mój dotychczas najbardziej zaawansowany projekt, który wymagał użycia map leaflet i zintegrowanie ich z geolokalizacyjnym i pogodowymAPI. Tworząc ten projekt, chciałem aby korzystanie z aplikacji było intuicyjne i ciekawe dla użytkownika. To właśnie forma, duża ilośc zgromadzonych danych, oraz możliwość interaktywnego korzystania z mapy, wyróżnia ten projekt na tle całej reszty projektów, pisanych z wykorzystaniem API pogodowego", "Interfejs graficzny aplikacji został wzbogacony o duże ilości zdjęć, a wewnętrzna baza danych zawiera dużą ilość informacji o wyszukiwanych miejscach. Wykorzystanie reacta pozwoliło mi na znacznie lepsze zarządzanie funkcjonalnością aplikacji, ułatwiając zmiane trybów wyszukiwania i obsługe błędów."],
        img: 'climate2',
        url: "https://optimistic-heisenberg-650ecb.netlify.com/",
        giturl:"https://github.com/goeden22/climate-map"
    },
    {
        index: 1,
        name: "Web-chat Client",
        tags: ["Node.js", "WebSocket", "Express", "Back-end", "Sass"],
        description: ["Aplikacja łącząca elementy front-endu i back-endu, napisana w node.js i javascript z wykorzystaniem technologii web-socket. W pełni funkcjonalny chat z możliwościa wyboru nazwy użytkownika, oraz pokoju. Implementacja web-socket pozwala na dwukierunkową komunikacje z serwerem w czasie rzeczywistym.", "Ze strony front-endu zadbałem o takie szczegóły jak inny wygląd wiadomości własnych i wiadomości innych użytkowników, możliwość wyboru avatara i pełną responsywność aplikacji, oraz atrakcyjną oprawę wizualną. Detale takie jak aktualizowana lista użytkowników wraz z czasem jaki minął od ich ostatniej wiadomości, oraz możliwość płynnego przechodzenia między pokojami, jest niewątpliwie urozmaiceniem", "Ze strony back endu aplikacja zapewnia pełną funkcjonalność i prawidłowe działanie chatu, oraz komunikacje pomiędzy clientem a serwerem, jest to także pierwsza aplikacja w której wykorzystałem framework Mocha do napisania testów."],
        img: 'chat',
        url: "https://protected-ocean-50450.herokuapp.com",
        giturl:"https://github.com/goeden22/web-chat-client"
    },
    {
        index: 2,
        name: "Boxing PPV Gallery",
        tags: ["React", "Transitions", "Animations", "Intervals"],
        description: ["Galeria poświęcona najbardziej dochodowym eventom w historii boksu zawodowego. Napisana z wykorzystaniem React-transitions. Projekt ten miał skupić się bardziej na wizualnym aspekcie front-endu niż na jego funckjonalności. Moje podejście do stworzenia cieszącej oko prezentacji w postaci strony internetowej, pełnej animacji i przedstawienia danych w przystępnej i atrakcyjnej formie.", "Dużym wyzwaniem okazało się tutaj połączenie automatycznego przewijania slajdów z jednoczesną możliwością nawigowania przez użytkownika, w taki sposób, aby wywołane jednocześnie animacje nie kolidowały ze sobą i nie bugowały aplikacji. Nauczyłem się przy tym projekcie dużo o przejściach, timeoutach i interwałach w JavaScript i CSS. Zapraszam do sprawdzenia."],
        img: 'boxing',
        url: "https://tender-ptolemy-06b2d8.netlify.com/",
        giturl:"https://github.com/goeden22/boxinggallerydeploy"
    },
    {
        index: 3,
        name: "Calisthenics website",
        tags: ["Bootstrap", "Sass", "Webpack"],
        description: ["Zwyczajna strona internetowa napisana z wykorzystaniem bootstrapa, sass i vanilla js. Dla mnie, napisanie jej stanowiło wtedy przede wszystkim nauke samodzielnego skonfigurowania webpacka w celu skompilowania kodu do jak najprostszej postaci."," Jeden z moich pierwszych projektów, który wrzucam tutaj, aby pokazać że oprócz używania frameworków i bibliotek, potrafie także stworzyć coś od zera, a zakodowanie prostego landing page'a również nie jest mi obce.","Wszystkie skrypty starałem się pisać z wykorzystaniem obiektów, aby kod był nieco czytelniejszy i łatwiejszy do modyfikacji"],
        img: 'cali',
        url: "https://goeden22.github.io/CalisthenicsWebsite/",
        giturl:"https://github.com/goeden22/CalisthenicsWebsite"
    }
]

module.exports = {data}